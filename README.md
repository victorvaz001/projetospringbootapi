# Api com SpringBoot JPA

## Tecnologias

* Java 1.08<br>
* Spring Boot<br>
* Rest, RestFull<br>
* JPA<br>
* Hibernate<br>
* PotgreSQL<br>

## Observações

* Classe com metodo main, com as anotações necessarias para trabalhar com Hibernate SpringBoot, Rest, SpringMvc<br>
* Projeto já está configurado para criar tabelas automaticas com Hibernate<br>
* Back-end<br>
* Classe modelo Usuario modelo<br>
* IndexController anotado com o @RestController para manipular os metodos REST e @RequestMapping para usuario<br>
* applications.properties configurado para trabalhar com o Hibernate e postgres<br>


## Testes
* Para testar o projeto, devera ter o postgres e criar um banco de dados com usuario e senha, alterar o no arquivo de persistencia
* Post man para testes da api
* Api encontra-se no meu site http://victorgoncalvesvaz.com.br/projetospringapi/usuarios/